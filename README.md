A java banking application that runs in the console.

Connects to a database and is usable by customers and employees.

It satisfies these user stories:
- As a user, I can login.
- As a customer, I can apply for a new bank account with a starting balance.
- As a customer, I can view the balance of a specific account.
- As a customer, I can make a withdrawal or deposit to a specific account.
- As the system, I reject invalid transactions.
- As an employee, I can approve or reject an account.
- As an employee, I can view a customer's bank accounts.
- A an employee, I can view a log of all transactions.
